# Traefik Proxy

## Setup

```sh
# Create an external docker network named 'proxy':
docker network create proxy
# Create an empty JSON file to store the SSL certificates:
touch acme.json
# Set the file permission of the JSON file to 600 (only editable by the file owner):
chmod 600 acme.json
# Start the containers:
docker compose up -d
```

See [example](./example) directory.

Read Traefik documentation: https://doc.traefik.io/traefik/
